﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Server
{
    class Program
    {
        private static IList<Socket> _clients = new List<Socket>();
        static void Main(string[] args)
        {
            var listener = new TcpListener(new IPEndPoint(0, 1994));
            listener.Start();
            for(;;)
            {
                var client = listener.AcceptSocket();
                lock (_clients)
                {
                    _clients.Add(client);
                }         
                BeginRecieve(new SocketData
                {
                    Socket = client
                });
            }
        }

        static void BeginRecieve(SocketData data)
        {
            data.Socket.BeginReceive(data.Buffer, 0, 1024, SocketFlags.None, new AsyncCallback(OnRecieve), data);
        }

        static void OnRecieve(IAsyncResult result)
        {
            var data = (SocketData)result.AsyncState;
            SocketError error;
            var read = data.Socket.EndReceive(result, out error);
            if (read > 0 && error == SocketError.Success)
            {
                lock (_clients)
                {
                    foreach (var client in _clients.Where(cl => cl != data.Socket))
                    {
                        client.Send(data.Buffer, 0, read, SocketFlags.None);
                    }
                }               
                BeginRecieve(data);
            } else
            {
                lock (_clients)
                {
                    _clients.Remove(data.Socket);
                }
                data.Socket.Close();
            }
        }
    }

    class SocketData
    {
        public byte[] Buffer { get; } = new byte[1024];

        public Socket Socket { get; set; }
    }
}
