﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CuteClient
{
    public partial class CuteForm : Form
    {
        private Client _client = new Client();
        public CuteForm()
        {
            InitializeComponent();
            _client.Connect();
            _client.Recieve += delegate(object sender, string message)
            {
                chatBox.Items.Add("Не я: " + message);
            };
            _client.Disconnect += delegate
            {
                MessageBox.Show(this, "Соединение с сервером разорвано", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Close();
            };
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(msgBox.Text))
            {
                MessageBox.Show(this, "Введите текст сообщения!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            _client.Send(msgBox.Text);
            chatBox.Items.Add("Я: " + msgBox.Text);
            msgBox.Clear();
        }
    }
}
