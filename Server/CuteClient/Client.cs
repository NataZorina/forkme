﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CuteClient
{
    public sealed class Client
    {
        private byte[] _buffer = new byte[1024];
        private Socket _socket;
        private IPEndPoint _endPoint;

        public event EventHandler<string> Recieve;
        public event EventHandler Disconnect;

        public Client(string host = "127.0.0.1", int port = 1994)
        {
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _endPoint = new IPEndPoint(IPAddress.Parse(host), port);
        }

        public void Connect()
        {
            _socket.Connect(_endPoint);
            BeginRecieve();
        }

        public void Send(string message)
        {
            _socket.Send(Encoding.UTF8.GetBytes(message));
        }

        private void BeginRecieve()
        {
            _socket.BeginReceive(_buffer, 0, 1024, SocketFlags.None, new AsyncCallback(Recieved), null);
        }

        private void Recieved(IAsyncResult result)
        {
            SocketError error;
            var read = _socket.EndReceive(result, out error);
            if (read > 0 && error == SocketError.Success)
            {
                var message = Encoding.UTF8.GetString(_buffer, 0, read);
                BeginRecieve();
                OnRecieve(message);
            } else
            {
                _socket.Close();
                OnDisconnect();
            }
        }

        private void OnRecieve(string message)
        {
            Recieve?.Invoke(this, message);
        }

        private void OnDisconnect()
        {
            Disconnect?.Invoke(this, null);
        }
    }
}
