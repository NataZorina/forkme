﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Threading;

namespace UnitTests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Test1()
        {
            var clients = Enumerable.Repeat(0, 100).Select(x => new CuteClient.Client()).ToArray();
            var recieveCount = 0;
            foreach (var client in clients)
            {
                client.Connect();
                client.Recieve += delegate (object sender, string message)
                {
                    Interlocked.Increment(ref recieveCount);
                    Assert.AreEqual(message, "сообщение");
                };
            }
            for(int i = 0; i < 1000; i++)
            {
                clients[0].Send("сообщение");
                Thread.Sleep(3);
            }
            
            Assert.AreEqual(recieveCount, 99 * 1000);
        }
    }
}
