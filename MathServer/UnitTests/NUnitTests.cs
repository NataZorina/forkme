﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    public class NUnitTests
    {
        [Test]
        public void Matrix()
        {
            var a = new Client.Client();
            string result = a.Send("1 1 1 1 1 1 1 1");
            Assert.AreEqual(result, "2 2 2 2");
        }
        [Test]
        public void Matrix2()
        {
            var a = new Client.Client();
            string result = a.Send("2 2 2 2 2 2 2 2");
            Assert.AreEqual(result, "8 8 8 8");
        }
        [Test]
        public void Matrix3()
        {
            var a = new Client.Client();
            string result = a.Send("2 2 2 2 2 2 2 2");
            Assert.AreEqual(result, "2 8 8 8");
        }
    }
}
