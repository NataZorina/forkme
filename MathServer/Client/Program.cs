﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    public class Program
    {
        static void Main(string[] args)
        {
            Client cl = new Client();
            for(;;)
            {
                Console.WriteLine("Введите матрицу: ");
                string matrix = Console.ReadLine();
                string result = cl.Send(matrix);
                Console.WriteLine(result);
            }
        }
    }
    public class Client
    {
        public Socket socket;
        IPEndPoint endPoint;
        public Client()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1994);
            socket.Connect(endPoint);
        }
        public string Send(string msg)
        {
            socket.Send(Encoding.UTF8.GetBytes(msg));
            SocketError error;
            var buffer = new byte[1024];
            var read = socket.Receive(buffer, 0, 1024, SocketFlags.None, out error);
            return Encoding.UTF8.GetString(buffer, 0, read);
        }
    }
}
