﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    public class Program
    {
        static void Main(string[] args)
        {
            var listener = new TcpListener(new IPEndPoint(0, 1994));
            listener.Start();
            for (;;)
            {
                var client = listener.AcceptSocket();
                BeginRecieve(new SocketData
                {
                    Socket = client
                });
            }
        }

        static void BeginRecieve(SocketData data)
        {
            data.Socket.BeginReceive(data.Buffer, 0, 1024, SocketFlags.None, new AsyncCallback(OnRecieve), data);
        }

        static void OnRecieve(IAsyncResult result)
        {
            var data = (SocketData)result.AsyncState;
            SocketError error;
            var read = data.Socket.EndReceive(result, out error);
            if (read > 0 && error == SocketError.Success)
            {
                var input = Encoding.UTF8.GetString(data.Buffer, 0, read);
                data.Socket.Send(Encoding.UTF8.GetBytes(DoSomething(input)));
                BeginRecieve(data);
            }
            else
            {
                data.Socket.Close();
            }
        }

        static string DoSomething(string input)
        {
            string[] arr = input.Split(' ');
            if (arr.Length != 8)
                return "Необходимо 2 матрицы 2 на 2";

            int a1 = Convert.ToInt32(arr[0]) * Convert.ToInt32(arr[4]) + Convert.ToInt32(arr[1]) * Convert.ToInt32(arr[6]);
            int a2 = Convert.ToInt32(arr[0]) * Convert.ToInt32(arr[5]) + Convert.ToInt32(arr[1]) * Convert.ToInt32(arr[7]);
            int a3 = Convert.ToInt32(arr[2]) * Convert.ToInt32(arr[4]) + Convert.ToInt32(arr[3]) * Convert.ToInt32(arr[6]);
            int a4 = Convert.ToInt32(arr[2]) * Convert.ToInt32(arr[5]) + Convert.ToInt32(arr[3]) * Convert.ToInt32(arr[7]);

            return a1.ToString() + " " + a2.ToString() + " " + a3.ToString() + " " + a4.ToString();
        }
    }

    class SocketData
    {
        public byte[] Buffer { get; } = new byte[1024];

        public Socket Socket { get; set; }
    }
}
